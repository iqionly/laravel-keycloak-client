<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Laravel\Socialite\Facades\Socialite;

class KeyCloakController extends Controller
{
    protected array $config;

    public function __construct()
    {
        $this->config = config('services.keycloak');
    }

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        return 'Login Successfully with account name: ' . $request->user()->name . ', timestamp: ' . now();
    }
    
    public function login(Request $request)
    {
        return view('login');
    }

    public function action_login(Request $request)
    {

    }

    public function callback(Request $request)
    {
        $keycloakUser = Socialite::driver('keycloak')->user();
        $user = User::updateOrCreate([
            'keycloak_id' => $keycloakUser->getId()
        ], [
            'name' => $keycloakUser->getName(),
            'email' => $keycloakUser->getEmail(),
            'password' => bcrypt($keycloakUser->getId()),
            'keycloak_token' => $keycloakUser->token,
            'keycloak_refresh_token' => $keycloakUser->refreshToken,
        ]);

        Auth::login($user);

        return redirect()->intended();
    }
}
