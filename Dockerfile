FROM php:8.3.8-fpm-alpine3.20

WORKDIR /var/www/html

ARG DB_CONNECTION=pgsql
ARG DB_HOST=127.0.0.1
ARG DB_PORT=5432
ARG DB_DATABASE=test_laravel_keycloak
ARG DB_USERNAME=postgres
ARG DB_PASSWORD=postgres

COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer

RUN apk update && apk add --no-interactive --no-progress supervisor

RUN echo "* * * * * cd /var/www/html && php artisan schedule:run" | crontab -

COPY .env.example .env

COPY . /var/www/html/

RUN chown -R www-data:www-data /var/www/html

USER www-data

RUN composer install --no-dev --no-interaction --no-progress --prefer-dist --no-suggest --optimize-autoloader

RUN php artisan key:generate
RUN php artisan optimize

EXPOSE 9000

CMD ["php-fpm"]