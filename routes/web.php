<?php

use Illuminate\Support\Facades\Route;
use Laravel\Socialite\Facades\Socialite;

use App\Http\Controllers\KeyCloakController;
 
Route::match(['get', 'post'], '/auth/redirect', function () {
    return Socialite::driver('keycloak')->redirect();
})->name('keycloak.redirect');
 
Route::get('/auth/callback', [KeyCloakController::class, 'callback'])->name('keycloak.callback');

Route::middleware(['auth'])->group(function () {
    Route::get('/', [KeyCloakController::class, 'index'])->name('index');
});
Route::get('/login', [KeyCloakController::class, 'login']);
Route::post('/login', [KeyCloakController::class, 'action_login'])->name('login');