<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('users', function(Blueprint $table) {
            $table->dateTime('last_login_at')->nullable();
            $table->dateTime('first_login_at')->nullable();
            $table->dateTime('logout_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::whenTableHasColumn('users', 'last_login_at', function(Blueprint $table) {
            $table->dropColumn('last_login_at');
            $table->dropColumn('first_login_at');
            $table->dropColumn('logout_at');
        });
    }
};
