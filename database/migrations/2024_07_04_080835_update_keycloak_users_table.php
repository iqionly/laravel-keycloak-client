<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('keycloak_id')->nullable()->unique();
            $table->string('keycloak_token')->nullable();
            $table->string('keycloak_refresh_token')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::whenTableHasColumn('users', 'keycloak_id', function(Blueprint $table) {
            $table->dropColumn('keycloak_id');
            $table->dropColumn('keycloak_token');
            $table->dropColumn('keycloak_refresh_token');
        });
    }
};
