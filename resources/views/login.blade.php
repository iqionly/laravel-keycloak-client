<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login Page</title>
</head>
<body>
    <form action="{{ route('keycloak.redirect') }}" method="post">
        @csrf
        <button type="submit" name="submit">Login KeyCloak</button>
    </form>
</body>
</html>